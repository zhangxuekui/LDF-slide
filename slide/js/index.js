/* 
* @Author: LDF QQ 47121862
* @Date:   2014-05-16 17:34:12
* @Last Modified by:   LDF QQ 47121862
* @Last Modified time: 2014-05-16 18:22:16
*/

$(document).ready(function(){
	new slide('#s1');
	var s2={
		time:1000,										//轮播时间单位秒
		border: 3,										//运动条边框粗细
		height: 10, 									//运动条高度
		actHeight: 10, 								//当前运动条高度
		borderColor: '#1099E0', 					//运动条边框颜色
		bgColor: '#18A2EA', 							//运动条背景颜色
		extendOutCss:'background:#2B5B99;', 	//运动条外框自定义css
	}
	new slided('#s2',s2);
	var s3={
		time:2000,					//轮播时间单位秒
		numHeight: 15, 			//运动条整体高度
		width: 45, 					//运动条宽度
		height: 10, 				//运动条高度
		actHeight: 14, 			//当前运动条高度
		position: 'bottom:8px', //运动条位置
		border: 1,					//运动条边框粗细
		borderColor: '#eee', 	//运动条边框颜色
		bgColor: '#fff', 			//运动条背景颜色
		extendOutCss:'background:#999;margin-right:10px;', //运动条外框自定义css
		extendCss:'box-shadow:inset 0 0 2px #eee,0 0 5px #eee,0 0 15px #eee;' //运动条扩展css
	}
	new slided('#s3',s3);
	
});