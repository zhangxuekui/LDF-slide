/* 
* @Author: LDF QQ 47121862
* @Date:   2014-05-11 13:18:21
* @Last Modified by:   LDF QQ 47121862
* @Last Modified time: 2014-05-16 18:17:59
*/
function slide(obj){
	this.id = $(obj);
	this.run();
	return this;
}
//初始化运行
slide.prototype.run=function(){
	this.setOpt().getDom().addNum().auto().addev();
	return this;
}
// 设置属性
slide.prototype.setOpt=function(){
	this.o={
		time:5000,					//轮播时间单位毫秒
		numHeight: 14, 			//运动条整体高度
		width: 60, 					//运动条宽度
		height: 10, 				//运动条高度
		actHeight: 14, 			//当前运动条高度
		bgColor: '#fff', 			//运动条背景颜色
		position: 'bottom:10px',//运动条位置
		borderColor: '#eee', 	//运动条边框颜色
		border: 0,					//运动条边框粗细
		extendOutCss:'background:#999;', 		//运动条外框自定义css
		extendCss:null				//运动条扩展css
	};
	this.c = 0; //当前图片
	this.timer=null;
	return this;
}
//获取DOM
slide.prototype.getDom=function(){
	this.a=this.id.children('.pic').children('a'); //所有图片a链接
	this.img=this.id.children('.pic').find('img'); //所有图片
	return this;
}
//获取新增滚动条DOM
slide.prototype.getNumDom=function(){
	this.num=this.id.children('.num'); //运动条
	this.n=this.id.children('.num').children('b'); //所有运动条
	return this;
}
//图片运动
slide.prototype.move=function(){
	this.a.eq(this.c).show().siblings().hide();
	return this;
}
//自动轮播
slide.prototype.auto=function(){
	var _this = this;
	this.numMove();
	clearInterval(this.timer);
	this.timer=setInterval(function(){
		_this.c++;
		if(_this.c>=_this.a.length) _this.c=0;
		_this.move().numMove();
	},Number(this.o.time))
	return this;
}
//运动条运动
slide.prototype.numMove=function(){
	this.resizeNum();
	this.n.css('opacity',.5);
	this.n.eq(this.c).css('opacity',1).children('i').stop().animate({'width':'100%'},Number(this.o.time),'linear',function(){
		$(this).css('width','0');
	})
	return this;
}
//当前运动条加高
slide.prototype.resizeNum=function(){
	this.n.eq(this.c).css('height',this.o.actHeight+'px').siblings().css('height',this.o.height+'px');
	return this;
}
//运动条停止
slide.prototype.stopNum=function(){
	this.n.eq(this.c).children('i').stop();
	return this;
}
//运动条擦除
slide.prototype.eraseNum=function(){
	this.n.eq(this.c).children('i').css('width','0');
	return this;
}
//添加运动条
slide.prototype.addNum=function(){
	var html="<div class='num' style='position: absolute; "+this.o.position+"; line-height:"+this.o.numHeight+"px; height:"+this.o.numHeight+"px; text-align: center; width:100%;'>",tmp='';	
	for(var i=0; i<this.a.length; i++){
		tmp+="<b style='display:inline-block;height:"+this.o.height+"px;width:"+this.o.width+"px;border:"+this.o.border+"px solid "+this.o.borderColor+";text-align: left;font-size:0;margin-right:5px;"+this.o.extendOutCss+"'><i style='vertical-align:middle;display: inline-block;background:"+this.o.bgColor+";height: 100%;font-size:0;width:0%;"+this.o.extendCss+"'>&nbsp;</i></b>";
	}
	html+=tmp;
	html+='</div>';
	this.id.append(html);
	this.getNumDom(); //获取新增加Dom
	return this;
}
//删除运动条
slide.prototype.delNum=function(){
	this.num.remove();
	return this;
}
//重载运动条,继承时候用
slide.prototype.reloadNum=function(){
	this.delNum().addNum().auto().addev();
	return this;
}
//添加基本事件
slide.prototype.addev=function(){
	var _this = this;
	//图片移入移出
	this.img.hover(function(){
		clearInterval(_this.timer);
		_this.stopNum();
	},function(){
		_this.auto();
	})
	//运动条移入移出
	this.n.hover(function(){
		clearInterval(_this.timer);
		_this.stopNum().eraseNum();
		$(this).children('i').css({'width':'100%'});
		_this.c=$(this).index();
		_this.resizeNum().move().numMove();
	},function(){
		_this.eraseNum().auto();
	})
	return this;
}
//=====================自定义外观轮播========================
function slided(obj,opt){
	//继承原属性
	slide.call(this,obj);
	if(opt) this.init(opt).reloadNum();
}
//继承原方法
for(var i in slide.prototype){
	slided.prototype[i]=slide.prototype[i];
}
//初始化新增参数
slided.prototype.init=function(opt){
	this.o = $.extend(this.o,opt);
	return this;
}
