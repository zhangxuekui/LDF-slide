轮播图插件
===================

仿1号店轮播图--老地方
-------------------
###基于JQuery，首先引入必要插件
	<script type="text/javascript" src='../js/jquery.js'></script>
	<script type="text/javascript" src='js/slide.js'></script>
	<script type="text/javascript" src='js/index.js'></script>
###调用方式
> 最基本的调用

	new slide('#s1');

> 效果图
![github](http://git.oschina.net/ldf/LDF-slide/raw/master/slide/img/s1.png "github")

> 自定义调用1

	var s2={
		time:1000,								//轮播时间单位秒
		border: 3,								//运动条边框粗细
		height: 10, 							//运动条高度
		actHeight: 10, 							//当前运动条高度
		borderColor: '#1099E0', 				//运动条边框颜色
		bgColor: '#18A2EA', 					//运动条背景颜色
		extendOutCss:'background:#2B5B99;', 	//运动条外框自定义css
	}
	new slided('#s2',s2);
###效果图
![github](http://git.oschina.net/ldf/LDF-slide/raw/master/slide/img/s2.png "github")
> 自定义调用2

	var s3={
		time:2000,				//轮播时间单位秒
		numHeight: 15, 			//运动条整体高度
		width: 45, 				//运动条宽度
		height: 10, 			//运动条高度
		actHeight: 14, 			//当前运动条高度
		position: 'bottom:8px', //运动条位置
		border: 1,				//运动条边框粗细
		borderColor: '#eee', 	//运动条边框颜色
		bgColor: '#fff', 		//运动条背景颜色
		extendOutCss:'background:#999;margin-right:10px;', //运动条外框自定义css
		extendCss:'box-shadow:inset 0 0 2px #eee,0 0 5px #eee,0 0 15px #eee;' //运动条扩展css
	}
	new slided('#s3',s3);
###效果图
![github](http://git.oschina.net/ldf/LDF-slide/raw/master/slide/img/s3.png "github")
###效果图总览
![github](http://git.oschina.net/ldf/LDF-slide/raw/master/slide/img/index.png "github")

Email: 47121862@.qq.com








